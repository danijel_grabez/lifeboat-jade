# Project_name

This is a Project_name repository which is built with [Lifeboat - Jade](https://bitbucket.org/danijel_grabez/lifeboat-jade).

### Installation

Install required grunt-related libraries with `npm install`.  
Install *resources and tools (global NPM packages)* mentioned bellow.  
Initialize Dploy with `dploy install` *(for FTP/SFTP code deployment)*.

### Building the assets

Run main grunt task with `grunt watch-files`.  
All assets source files are located in `/source/less, js, images` and built files are located in `/assets/css, js, images`.  
Pug pages are located in `source/site-pages`. This task builds HTML pages in the root directory.  

**Handy tasks:**  
1. `grunt copy-assets` – copy fonts, favicons and SVG files which are not handled by `watch` task.  
2. `grunt copy-javascript` – copy all JavaScript plugins used in project to `assets/js/unminified-scripts/`. This task is handy in situations when the repo needs to be passed to developers who are integrating the files without Grunt setup.  

### Resources and tools

- [LESS](http://lesscss.org/) — CSS Preprocessor
- [Pug](https://pugjs.org/api/getting-started.html) — Pug Templating Language
- [NPM](https://nodejs.org/) — Node Package Manager
- [Grunt.js](http://gruntjs.com/) — JavaScript Task Runner
- [BrowserSync](https://www.browsersync.io/) — Time-saving synchronised browser testing
- [DPLOY](http://lucasmotta.github.io/dploy/) — Deployment Tool
- [Surge](https://surge.sh/) — Deploy projects to a production-quality CDN 

For detailed information about code convention and how this project is built see [Styleguide](_styleguide.html) page. For brief information about each of the mentioned tool see [Tools and Services Page](documentation/tools-and-services.md).

### Author
Danijel Grabež  
website: www.danijelgrabez.com  
email: danijel889@gmail.com
