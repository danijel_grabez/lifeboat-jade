# Project_name Change Log
All notable changes to this project will be documented in this file.

---

## [MAJOR.MINOR.PATCH] - YYYY-MM-DD
### Added
- Add animation mixin.

### Changed
- Update year to match in every README example.

### Removed
- Old CSS styles from _scaffolding.less.

### Fixed
- Typos in recent README changes.

---


## [Unreleased]


## [0.0.0] - 2016-08-21
### Added
- Changelog (it is time to start tracking changes in this way as well).
- Tag functionality.


