# 🆕 TODO List                                [14 •]
    List item #1  
    List item #2  
    List item #3  



# ❔ Questions
    ☐ List item #1  
    ☐ List item #2  



# 🕓 Later    
    ☐ Initial steps
      ☐ Update Grunt tasks and package.json content
      ☐ Update documentation (README.md, _styleguide, tools-and-services.md)
      ☐ Update favicons
      ☐ Create SublimeText project and sincronize with PlainNotes plugin
      ☐ Run grunt copy in case fonts, svg and similar files are added



# 📁 Archived
~~DD/MM/YYYY            [14 •]    
  ✔ List item #1    
  ✔ List item #2    
  ✗ List item #3~~    



# 📎 Additional Notes
Jot down additional information about the project.



# 💡 About .note documents
This file is created with Plain Notes plugin for Sublime Text 3.
In order to enable saving notes inside /documentation/notes/ folder you will need to change the preferences by adding the following in your **.sublime-project** file:

    "settings":
        {
            "PlainNotes": 
            {
                "root": "path/to/project/documentation/notes/"
            }
        }



# 💡 Task status icons
    To do           ☐
    Checked         ✔
    Unfinished      ✗
    Urgent          ⚑
    
        ¯\_(ツ)_/¯
