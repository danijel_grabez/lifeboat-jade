
module.exports = function(grunt) {

  grunt.initConfig({

    //===========================================================================================================
    // grunt-contrib-pug
    //
    // grunt-contrib-pug compile pug files into html files.
    //
    // First task compiles all .pug files from site-pages folder to HTML (partials folder holds repeating page elements,
    // which are included within .pug pages. Pug files from this folder will not be compiled to HTML.)
    //
    // Second task compiles svg holder html page which holds all svg icons. This file will be included additionaly
    // in all HTML pages with JavaScript and it will be cached in local storage.
    //
    //===========================================================================================================

    pug: {
      compile: {
        files: [{
          expand: true,
          flatten: true,
          cwd: 'source/site-pages/',                   // Starting location of .pug files.
          src: ['*.pug', '!**config.pug**', '!**pagewrapper.pug**'],
          dest: '.',                                   // Destination of compiled .html files.
          ext :'.html'
        }],
        options: {
          pretty: false,                                // Keep the .html files minified/unminified.
          basedir: 'source/site-pages/partials'         // Location of .pug partials.
        }
      },
      compileSvgHolder: {
        options: {
          data: {
            debug: false
          }
        },
        files: {
          'assets/images/icons/svg-holder.html': ['source/site-pages/partials/svg-holder.pug']
        }
      }
    },
    //===========================================================================================================
    // grunt-contrib-less
    //
    // grunt-contrib-less compile less files into css files.
    // Two less tasks are defined:
    // Development task creates unminified css file with the sourcemap.
    // Deploy task creates minified css file.
    //===========================================================================================================

    less: {
      development: {                      // Compile unminified css.
        options: {
          banner: '/* Author: Danijel Grabež */\n' // Banner will be added at the top of the compiled file.
        },
        files: {                          // Dictionary of files (destination : start location).
          'assets/css/style.css': 'source/less/style.less'
        }
      },

      deploy: {                            // Compile minified css.
        options: {
          compress: true,                  // Css output style.
          banner: '/* Author: Danijel Grabež */\n' // Banner will be added at the top of the compiled file.
        },
        files: {                           // Dictionary of files (destination : start location).
          'assets/css/style.min.css': 'source/less/style.less',
          'assets/css/lifeboat.min.css': 'source/less/lifeboat.less'
        }
      }
    },
    //===========================================================================================================
    // grunt-postcss
    //
    // grunt-postcss task run autoprefixing for compiled css files.
    // This task depends on autoprefixer-core which provide css prefixes based on defined browser support.
    //===========================================================================================================

    postcss: {
      options: {
        processors: [
          require('autoprefixer-core')({browsers: '> 1%'}),      // autoprefixer core represents dependency package.
        ]
      },
      dist: {
        src: 'assets/css/*.css'                                  // destination of css files which will be prefixed.
      }
    },
    //===========================================================================================================
    // grunt-contrib-jshint
    //
    // grunt-contrib-jshint task look for possible errors in the javascript files.
    // jshint-stylish provide prettier console output.
    //===========================================================================================================

    jshint: {
      all: 'source/js/components/**/*.js',
      options: {
        reporter: require('jshint-stylish'),
        eqeqeq: false,                                 // Prohibit the use of == and != in favor of === and !==
        curly: true                                    // Require you to always put curly braces around blocks in loops and conditionals.
      }
    },
    //===========================================================================================================
    // grunt-contrib-uglify
    //
    // grunt-contrib-uglify task concatintate, minify and uglify javascript files.
    // Two uglify tasks are defined:
    // jsbase uglify task compiles support scripts for older browsers.
    // js uglify task compiles plugin(s) which are used througout the project along with the custom javascript.
    //===========================================================================================================

    uglify: {
      jsbase: {
        options: {
          preserveComments: 'none'                             // Remove unnecessary comments.
        },
        files: [{
          src: [                                               // Jquery fallback (in case that cdn version is not loaded).
            'source/js/vendor/jquery/jquery-1.11.2.min.js'
          ],
          dest: 'assets/js/jquery-1.11.2.min.js'
        },
        {
          src: [                                               // Modernizr (custom made).
            'source/js/vendor/modernizr/modernizr.min.js'
          ],
          dest: 'assets/js/modernizr.min.js'
        }]
      },
      js: {
        options: {
          preserveComments: 'some',                            // Keep the comments.
          banner: '/* Author: Danijel Grabež */\n'             // Banner will be added at the top of the compiled file.
        },
        files: [{
          src: [                                               // List of plugins which are used on this project, along with
            'source/js/components/main.js'                     // custom made javascript.
          ],
          dest: 'assets/js/script.min.js'
        }]
      }
    },
    //===========================================================================================================
    // grunt-contrib-imagemin
    //
    // grunt-contrib-imagemin optimize images for the web
    // (.png, .jpeg, .svg, .gif are possible to be optimized).
    //===========================================================================================================

    imagemin: {
      png: {
        options: {
          optimizationLevel: 3             // Compression level.
        },
        files: [{
          expand: true,                    // Dynamic expansion.
          cwd: 'source/images/',
          src: ['**/*.png'],
          dest: 'assets/images',
          ext: '.png'
        }]
      },
      jpg: {
        options: {
          progressive: true                 // Lossless or progressive conversion.
        },
        files: [{
          expand: true,
          cwd: 'source/images/',
          src: ['**/*.jpg'],
          dest: 'assets/images',
          ext: '.jpg'
        }]
      }
    },
    //===========================================================================================================
    // grunt-svgstore
    //
    // grunt-svgstore task create svg sprite from source/images/icons folder.
    //
    // Description:
    // Generated svg sprite can be found inside assets/images/icons/ folder.
    // Svg code needs to be added inside .svg-holder class which should be placed right bellow the opening <body> tag:
    // <div class="svg-holder"> SVG SPRITE CODE GOES HERE </div>
    //
    // A file named svg-holder.pug should hold the svg code. This file can be found inside source/site-pages/partials folder.
    // Page which contains svg icons will need to have svg-holder.pug included (via `include svg-holder.pug` line).

    // At the moment only modified .pug files are compiled to .html files (this is enabled with grunt-newer task).
    // In order to "refresh" the content inside .svg-holder element across other pages newer: will need to be removed from the pug task.

    svgstore: {
      icons: {
        files: {
          'assets/images/icons/icons.svg': ['source/images/icons/*.svg']
        },
        options: {
          prefix: 'icon-',                      // add prefix to all icons with an unambiguous label

          cleanup: true,                        // cleans fill, stroke, stroke-width attributes
                                                // so that we can style them from the css.

          convertNameToId: function(name) {     // write a custom function to strip the first part
            return name.replace(/^\w+\_/, '');  // of the file that Adobe Illustrator generates
          }                                     // when exporting the artboards to SVG.
        }
      }
    },
    //===========================================================================================================
    // grunt-contrib-copy
    //
    // grunt-contrib-copy task copy files from one location to the another.

    copy: {
      assetsJavaScript: {                       // Copy all JavaScript plugins used in project to assets/js/unminified-scripts folder.
        files: [{                               // This task is handy in situations when the repo needs to be passed to developers
          expand: true,                         // who are integrating the files without Grunt setup.
          cwd: 'source/js',
          src: ['**', '!**vendor/jquery/**', '!**vendor/modernizr/**', '!**components/helper-scripts/**'],
          dest: 'assets/js/unminified-scripts'
        }]
      },
      assetsFavIcons: {                         // Copy fonts, favicons and SVG files (outside of images/icons folder) to assets folder.
        files: [{
          expand: true,
          cwd: 'source/images/favicons',
          src: ['**'],
          dest: 'assets/images/favicons'
        }]
      },
      assetsSVG: {
        files: [{
          expand: true,
          cwd: 'source/images',
          src: ['*.svg', '!**icons/**'],
          dest: 'assets/images/'
        }]
      },
      assetsFonts: {
        files: [{
          expand: true,
          cwd: 'source/fonts',
          src: ['**'],
          dest: 'assets/fonts/'
        }]
      }

    },
    //===========================================================================================================
    // grunt-contrib-watch
    //
    // grunt-contrib-watch task run tasks whenever watched files change.
    // Livereload option enables browser refreshing when this task is run
    // (livereload extension needs to be enabled on your browser).
    // grunt-watch action triggers with 'grunt watch-files' which listen file changes and triggers
    // certain action (in this case we are listening to the html, css, javascript, image and svg files).
    //===========================================================================================================

    watch: {
      options: {
        livereload: true                        // Show changes on browser without page refreshing.
      },

      html: {                                   // Refresh the browser when there are changes in html, css or js.
        files: ['source/site-pages/**/*.pug', 'source/less/**/*.less', 'source/js/**/*.js'],
        tasks: ['compile-html'],                // This task includes html partials from source/pages/layout
        options: {                              // and then compiles html pages in root directory.
          spawn: false,
          livereload: true
        }
      },

      css: {
        files: ['source/less/**/*.less'],       // When file from source/less folder is changed, run compile-css action.
        tasks: ['compile-css'],                 // This task compiles less into css, and then run postcss task.
        options: {                              // With combined actions we have to make correct task completion order
          spawn: false                          // (compile css and then postcss for autoprefixing).
        }
      },

      scripts: {                                // When file from source/js folder is changed, run compile-js action
        files: ['source/js/**/*.js'],           // (uglify and jshint tasks combined).
        tasks: ['compile-js'],
        options: {
          spawn: false
        }
      },

      imagemin: {                               // When we add, or change image from source/images folder, imagemin task is run.
        files: ['source/images/**/*.jpg', 'source/images/**/*.png'],
        tasks: ['optimize-images']
      },

      svgstore: {                               // When svg file is added, or modified from the source/images/icons folder
        files: ['source/images/icons/*.svg'],   // run svgstore task which will create new svg sprite in root/images/icons folder.
        tasks: ['svgstore']
      }
    },
    //===========================================================================================================
    // grunt-browser-sync
    //
    // grunt-browser-sync will synchronize the browser upon file changes.
    // When the files are compiled by watch task, browser will refresh the page.
    //===========================================================================================================

    browserSync: {
      synchronize: {
        bsFiles: {                                 // Reload the browser when these files are changed
          src : [
            '*.html',
            'assets/images/icons/svg-holder.html', // SVG icons (served via javascript to the localstorage)
            'assets/css/*.css',
            'assets/js/*.js'
          ]
        },
        options: {
          watchTask: true,                         // Synchronize with grunt-contrib-watch task
          server: './',                            // (when watch tasks compile files, browser will be reloaded)
          ui: false,                               // Don't provide browser sync user interface (that is accessed via a separate port)
          port: 9000,
          notify: false,                           // Don't show any notifications in the browser.
          logLevel: 'silent',                      // Don't show browsersync log messages
          tunnel: 'lifeboat',                      // Tunnel the Browsersync server through --> http://lifeboat.localtunnel.me (tunnel name can't contain hyphens, or upper cases)
          online: true                             // Some features of Browsersync (such as xip & tunnel) require an internet connection,
                                                   // but if you're working offline, you can reduce start-up time by setting this option to `false`.
        }
      }
    }

  });

  // Measures the time each task takes.
  // Does not work with grunt watch task, only separate task(s).
  require('time-grunt')(grunt);


  // Load installed plugins.
  // All installed plugins which are used in this project.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-svgstore');
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-browser-sync');

  grunt.loadNpmTasks('grunt-newer');
  // Grunt-newer plugin increases task running speed.
  // Settings require only adding 'newer:' as the first argument when running other tasks
  // (see registered tasks bellow - only images for now).


  //Run Tasks in specific order
  // All task(s) - Runs with 'grunt' command.
  grunt.registerTask('default', ['pug', 'uglify', 'jshint', 'less', 'postcss', 'imagemin', 'svgstore', 'copy-assets']);
  // Build html files from pug files.
  grunt.registerTask('compile-html', ['pug']);
  // Compile less files (first compile less into css, and then run postcss task).
  grunt.registerTask('compile-css', ['less', 'postcss']);
  // Compile main js files (first run uglify task, and then run jshint task).
  grunt.registerTask('compile-js', ['uglify:js', 'jshint']);
  // Compile base js files (fallback for older browsers). This task is already built and scripts are
  // placed inside assets/js folder; grunt-watch doesn't watch for changes which are defined within this task.
  grunt.registerTask('compile-js-base', ['uglify:jsbase', 'jshint']);
  // Optimize images.
  grunt.registerTask('optimize-images', ['newer:imagemin']);
  // Generate SVG sprites.
  grunt.registerTask('icons', ['svgstore']);
  // Copy JavaScript files to production
  grunt.registerTask('copy-javascript', ['copy:assetsJavaScript']);
  // Copy asset files (fonts, favicons and SVG files outside of images/icons folder)
  grunt.registerTask('copy-assets', ['copy:assetsFavIcons','copy:assetsSVG','copy:assetsFonts']);


  // Main task:
  // 1. 'grunt watch-files' - watch file changes and refresh the browser upon these changes.
  grunt.registerTask('watch-files', ['browserSync', 'watch']);

};
