$(document).ready(function() {

  // ===========================================================================
  //
  // Dropdown
  //
  // Example markup:
  //
  // .dropdown-environment
  //   a(href="#dropdown-id" class="js-dropdown-trigger dropdown-trigger") Click Me
  //
  //   #dropdown-id.dropdown
  //     .dropdown-arrow
  //     Dropdown Content

  $('.js-dropdown-trigger').on('click', function(e)  {
    // Show/Hide dropdown
    $('.dropdown').filter(this.hash).toggleClass('is-open');
    // Change/Remove dropdown trigger active state
    $(this).toggleClass('is-active');
    e.preventDefault();
  });

}); // end document ready
