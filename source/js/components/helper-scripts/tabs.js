$(document).ready(function() {

  // ===========================================================================
  //
  // Controls for a tabbed interface
  // Source: http://inspirationalpixels.com/tutorials/creating-tabs-with-html-css-and-jquery
  //
  // Example markup:
  //
  // .tab-environment
  //   nav.tab-nav
  //     ul.tab-nav__list
  //       li.tab-nav__item
  //          a(href="#tab1" class="tab-nav__link").is-active Tab #1
  //       li.tab-nav__item
  //          a(href="#tab2" class="tab-nav__link") Tab #2
  //       li.tab-nav__item
  //          a(href="#tab3" class="tab-nav__link") Tab #3
  //       li.tab-nav__item
  //          a(href="#tab4" class="tab-nav__link") Tab #4
  //
  //   .tab-group
  //     #tab1.tab-item.is-active
  //       p Tab #1 content goes here!
  //
  //     #tab2.tab-item
  //       p Tab #2 content goes here!
  //
  //     #tab3.tab-item
  //       p Tab #3 content goes here!
  //
  //     #tab4.tab-item
  //       p Tab #4 content goes here!

  $('.tab-nav__link').on('click', function(e)  {
    var currentAttrValue = $(this).attr('href');

    // Show/Hide Tabs
    $('.tab-environment ' + currentAttrValue).fadeIn(400).siblings().hide();

    // Change/remove current tab to active
    $(this).closest('.tab-nav__list').find('.tab-nav__link').removeClass('is-active');
    $(this).addClass('is-active');

    e.preventDefault();
  });

}); // end document ready
