$(document).ready(function() {

  // ===========================================================================
  //
  // Center content vertically
  //
  // Example markup:
  //
  // .container-class
  //  .center-content
  //    Content which is going to be centered.

  $(window).resize(function() {
    $('.center-content').css({
      'padding-top' : ($('.container-class').height()/2)-($('.center-content').height()/2)
    });
  });
  $(window).resize();


}); // end document ready
