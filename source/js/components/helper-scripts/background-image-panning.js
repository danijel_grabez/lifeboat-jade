$(document).ready(function() {

  // ===========================================================================
  //
  // Background Image Panning

  function screenSize(){
    if ($(window).width() >= 990) {
      panning();
    }
  }

  function panning(){
    $('.image-panning').mousemove(function (e) {
      var mousePosX = (e.pageX / $(window).width()) * 100;
      $('.image-panning').css('backgroundPosition', (mousePosX / 5) + '%' + '100%');
    });
  }
  screenSize();

  $(window).resize(function() {
    screenSize();
  });


}); // end document ready
