$(document).ready(function() {

  // ===========================================================================
  //
  // Modal
  //
  // Example markup:
  //
  // div(id="modal-id" class="modal" role="alert")
  //   .modal__container
  //     .modal__header
  //       h1 Modal header content
  //     .modal__content
  //       p Modal content
  //     .modal__footer
  //       p Modal footer content
  //
  //     a(href="#modal-id" class="modal__close js-modal-close")
  //       svg.icon
  //         use(xlink:href="#icon-close" xmlns:xlink="http://www.w3.org/1999/xlink")
  //
  // a(href="#modal-id" class="js-modal-trigger") Trigger Modal 1

  // Open modal
  $('.js-modal-trigger').on('click', function(event){
    event.preventDefault();
    $('.modal').filter(this.hash).addClass('is-visible');
    $('body').addClass('js-modal-active');
  });

  // Close modal
  $('.modal').on('click', function(event){
    if( $(event.target).is('.js-modal-close') || $(event.target).is('.modal') ) {
      event.preventDefault();
      $(this).removeClass('is-visible');
      $('body').removeClass('js-modal-active');
    }
  });

  // Close modal when clicking the esc keyboard button
  $(document).keyup(function(event){
    if(event.which=='27'){
      $('.modal').removeClass('is-visible');
      $('body').removeClass('js-modal-active');
    }
  });

}); // end document ready
