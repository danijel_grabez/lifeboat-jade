$(document).ready(function() {

  // ===========================================================================
  //
  // SVG Fallback for older browsers

  if (!Modernizr.svg) {

    $('img[src$=".svg"]').each(function() {
      //Replace 'image.svg' with 'image.png'.
      $(this).attr('src', $(this).attr('src').replace('.svg', '.png'));
    });

  }

}); // end document ready
