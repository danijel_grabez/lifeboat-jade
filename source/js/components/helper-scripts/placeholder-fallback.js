$(document).ready(function() {

  // ===========================================================================
  //
  // Placeholder attribute support for browsers that don't support it natively.

  jQuery(function() {
    jQuery.support.placeholder = false;
    test = document.createElement('input');
    if('placeholder' in test) {
      jQuery.support.placeholder = true;
    }
  });

  $(function() {
    if(!$.support.placeholder) {
      var active = document.activeElement;
      $(':text').focus(function () {
        if ($(this).attr('placeholder') !== '' && $(this).val() === $(this).attr('placeholder')) {
          $(this).val('').removeClass('hasPlaceholder');
        }
      }).blur(function () {
        if ($(this).attr('placeholder') !== '' && ($(this).val() === '' || $(this).val() === $(this).attr('placeholder'))) {
          $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
        }
      });
      $(':text').blur();
      $(active).focus();
      $('form').submit(function () {
        $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
      });
    }
  });

}); // end document ready
