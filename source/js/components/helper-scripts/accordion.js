$(document).ready(function() {

  // ===========================================================================
  //
  // Controls for accordion
  // Source: http://inspirationalpixels.com/tutorials/creating-an-accordion-with-html-css-jquery
  //
  // Example markup:
  //
  // .accordion
  //   a(href="#accordion-1").accordion__title Accordion Section #1
  //   a(href="#accordion-2").accordion__title Accordion Section #2
  //   a(href="#accordion-3").accordion__title Accordion Section #3
  //
  //  #accordion-1.accordion__content
  //    p Section Content 1.
  //
  //  #accordion-2.accordion__content
  //    p Section Content 2.
  //
  //  #accordion-3.accordion__content
  //    p Section Content 3.

  function closeAccordionSection() {
    $('.accordion__title').removeClass('is-active');
    $('.accordion__content').slideUp(400).removeClass('is-open');
  }

  $('.accordion__title').click(function(e) {
    // Grab current anchor value
    var currentAttrValue = $(this).attr('href');

    if($(e.target).is('.is-active')) {
      closeAccordionSection();
    }else {
      closeAccordionSection();

      // Add is-active class to section title
      $(this).addClass('is-active');
      // Open up the hidden content panel
      $('.accordion ' + currentAttrValue).slideDown(400).addClass('is-open');
    }

    e.preventDefault();
  });

}); // end document ready
