// ========================================================================
// Overlay Modal Effect
// ========================================================================

// Usage Example:
//
// Pug:
//
// #overlay-modal-id.js-overlay-modal.overlay-modal
//   .overlay-modal__container
//     .overlay-modal__header
//       a(href="#overlay-modal-id" class="overlay-modal__close js-overlay-modal-close")
//         svg.icon
//           use(xlink:href="#icon-close" xmlns:xlink="http://www.w3.org/1999/xlink")
//       h4 Overlay content.
//     .overlay-modal__content
//       span Overlay modal content.

//     .overlay-modal__footer
//       span Overlay modal footer.

// a(href="#overlay-modal-id").js-overlay-modal-toggle Toggle Overlay
//
// ========================================================================

$(document).ready(function() {

  var overlayToggle = $('.js-overlay-modal-toggle');
  var overlayClose = $('.js-overlay-modal-close');
  var overlays = $('.overlay-modal');
  var body = $('body');
  var overlayContent = $('.overlay-modal__container');

  // ===========================================================================
  //
  // Handle showing and hiding the modal
  overlayToggle.click(function(e) {

    // ===========================================================================
    //
    // Disable scrolling on body
    body.addClass('js-overlay-modal-active');

    // ===========================================================================
    //
    // Show targeted overlay
    overlays.filter(this.hash).addClass('js-overlay-modal-in');

    e.preventDefault();

  });

  // ===========================================================================
  //
  // Close the overlay
  overlayClose.click(function(e) {

    // ===========================================================================
    //
    // Re-enable scrolling on body
    body.removeClass('js-overlay-modal-active');

    // ===========================================================================
    //
    // Hide targeted overlay
    overlays.filter(this.hash).removeClass('js-overlay-modal-in');

    e.preventDefault();

  });


  // Close modal
  $('.overlay-modal').on('click', function(event){
    if( $(event.target).is('.js-overlay-modal-close') || $(event.target).is('.overlay-modal') ) {
      event.preventDefault();
      $(this).removeClass('js-overlay-modal-in');
      $('body').removeClass('js-overlay-modal-active');
    }
  });

  // Close modal when clicking the esc keyboard button
  $(document).keyup(function(event){
    if(event.which=='27'){
      $('.overlay-modal').removeClass('js-overlay-modal-in');
      $('body').removeClass('js-overlay-modal-active');
    }
  });

});
